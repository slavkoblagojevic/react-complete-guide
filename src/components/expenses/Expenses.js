import React from "react";
import ExpenseItem from "./ExpenseItem";

function Expenses(props) {
  let expenses = props.items;

  return (
    <div>
      {expenses.map((exp) => {
        return (
          <ExpenseItem key={exp.id} title={exp.title} amount={exp.amount} date={exp.date} />
        );
      })}
    </div>
  );
}

export default Expenses;
