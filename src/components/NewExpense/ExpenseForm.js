import React, { useState } from "react";
import "./ExpenseForm.css";

const ExpenseForm = (props) => {
  //Option 1. Multiple States
  //Option 2. One State
  //Option 3. One State (when depends on previous state)

  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredAmount, setEnetredAmount] = useState("");
  const [enteredDate, setEnetredDate] = useState("");
  const [isValid, setIsValid] = useState(true);

  /*   const [userInput, setUserInput] = useState({
    enteredTitle: "",
    enteredAmount: "",
    enteredDate: "",
  });
 */
  const titleChangeHandler = (event) => {
    if(event.target.value.trim().length > 0){
      setIsValid(true);
    }
    setEnteredTitle(event.target.value);
    /*     setUserInput({
      ...userInput,
      enteredTitle: event.target.value,
    }); */
    /*     setUserInput((prevState) => {
      return {...prevState, enteredTitle: event.target.value}
    });
 */
  };

  const amountChangeHandler = (event) => {
    if(event.target.value.trim().length > 0){
      setIsValid(true);
    }
    setEnetredAmount(event.target.value);
    /*     setUserInput({
      ...userInput,
      enteredAmount: event.target.value,
    }); */
  };

  const dateChangeHandler = (event) => {
    if(event.target.value.trim().length > 0){
      setIsValid(true);
    }
    setEnetredDate(event.target.value);
    /*     setUserInput({
      ...userInput,
      enteredDate: event.target.value,
    }); */
  };

  const submitHandler = (event) => {
    event.preventDefault();

    if (
      enteredTitle.trim().length === 0 ||
      enteredAmount.trim().length === 0 ||
      enteredDate.trim().length === 0
    ) {
      setIsValid(false);
      return;
    }

    const expenseData = {
      title: enteredTitle,
      amount: enteredAmount,
      date: new Date(enteredDate),
    };

    setEnteredTitle("");
    setEnetredAmount("");
    setEnetredDate("");

    props.onSaveExpenseData(expenseData);

    console.log(expenseData);
  };

  return (
    <form onSubmit={submitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label style={{color: !isValid ? 'red' : 'black'}}>Title</label>
          <input
            style={{borderColor: !isValid ? 'red' : null}}
            type="text"
            value={enteredTitle}
            onChange={titleChangeHandler}
          />
        </div>
        <div className="new-expense__control">
          <label style={{color: !isValid ? 'red' : 'black'}}>Amount</label>
          <input
          style={{borderColor: !isValid ? 'red' : null}}
            type="number"
            min="0.01"
            step="0.01"
            value={enteredAmount}
            onChange={amountChangeHandler}
          />
        </div>
        <div className="new-expense__control">
          <label style={{color: !isValid ? 'red' : 'black'}}>Date</label>
          <input
            style={{borderColor: !isValid ? 'red' : null}}
            type="date"
            min="2019-01-01"
            max="2022-12-31"
            value={enteredDate}
            onChange={dateChangeHandler}
          />
        </div>
      </div>
      <div className="new-expense__actions">
        <button type="submit">Add Expense</button>
      </div>
    </form>
  );
};

export default ExpenseForm;
